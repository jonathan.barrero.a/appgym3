import * as firebase from 'firebase/app';
import 'firebase/auth';

const app = firebase.initializeApp({
    apiKey: "AIzaSyDAoie1xXkh-_9aVuQBcNxDQwap8w-hYgQ",
    authDomain: "apptest-d99af.firebaseapp.com",
    databaseURL: "https://apptest-d99af.firebaseio.com",
    projectId: "apptest-d99af",
    storageBucket: "apptest-d99af.appspot.com",
    messagingSenderId: "226713527705",
    appId: "1:226713527705:web:71824eed73433eba3d5f08"
  });

// const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
// const facebookAuthProvider = new firebase.auth.FacebookAuthProvider();
// const githubAuthProvider = new firebase.auth.GithubAuthProvider();

export { app }

