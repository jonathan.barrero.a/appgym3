import React from 'react';
import './App.css';
import  { Login, Register }  from '../login/index';
import Home from '../home/home';
import { app } from '../../firebase/firebaseConfig';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogginActive: true,
      user: {}
    }
  }

  componentDidMount()
  {
    this.authListener();
  }

  authListener(){
    app.auth().onAuthStateChanged((user) => {
      if(user)
      {
        this.setState({user})
      }
      else{
        this.setState({user : null})
      }
    })
  }

  changeState() {
    const { isLogginActive } = this.state;
    if(isLogginActive) {
      this.rightSide.classList.remove("right");
      this.rightSide.classList.add("left");
    }else{
      this.rightSide.classList.remove("left");
      this.rightSide.classList.add("right");
    }
    this.setState((prevState) => ({ isLogginActive: !prevState.isLogginActive }))
  }

  render() {
    const { isLogginActive } = this.state;
    const current = isLogginActive ? 'Register' : 'Login';
    const currentActive = isLogginActive ? 'login' : 'register';
    
    if(this.state.user) {
      return <Home user={this.state.user} />
    }
    return (
      <div className="App">
        <div className="login">
          <div className="container">
            { isLogginActive && <Login containerRef={(ref) => this.current = ref }/> }
            { !isLogginActive && <Register containerRef={(ref) => this.current = ref }/> }
          </div>
          <RightSide
            current={current}
            currentActive={currentActive}
            containerRef={ref => (this.rightSide = ref)}
            onClick={this.changeState.bind(this)}
          />
        </div>
      </div>
    );
  }
}

const RightSide = props => {
  return (
    <div className="right-side" ref={props.containerRef} onClick={props.onClick}>
      <div className="inner-container">
        <div className="text">{props.current}</div>
      </div>
    </div>    
  );
}

export default App;
