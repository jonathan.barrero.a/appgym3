/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import { app } from '../../firebase/firebaseConfig';

export class Login extends React.Component {
    // eslint-disable-next-line no-useless-constructor    
    constructor(props) {
        super(props);
        this.login = this.login.bind(this);
        this.handleChange = this.handleChange.bind(this);        
        this.state = {
            email: "",
            password: ""
        }
    }    

    login(e) {
        e.preventDefault();
        app.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
            .then((u) => {
                console.log('ingreso bueno ');
            })
            .cath((err) => {
                console.log('Mal' + err.toString());
            });
    }        

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <div className="base-container">
                <div className="header">Login</div>
                <div className="content">
                    <div className="image">
              
                    </div>
                    <div className="form">
                        <div className="form-group">
                            <label>Username</label>
                            <input                                 
                                type="text" 
                                name="email" 
                                placeholder="Ingrese usuario" 
                                onChange={this.handleChange}
                                value={this.state.email} 
                                />
                        </div>
                        <div className="form-group">
                            <label>Password</label>
                            <input  
                                type="password" 
                                name="password" 
                                placeholder="Ingrese Password"                                 
                                onChange={this.handleChange}
                                value={this.state.password} 
                                />
                        </div>
                    </div>
                </div>
                <div className="footer">
                    <button onClick={this.login} className="btn" type="button">Login</button>
                </div>                
            </div>
        );
    }
}

