/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
// import LogoImage from '..';
import { app } from '../../firebase/firebaseConfig';

export class Register extends React.Component {
    // eslint-disable-next-line no-useless-constructor
    constructor(props) {
        super(props);        
        this.handleChange = this.handleChange.bind(this);
        this.signUp = this.signUp.bind(this);
        this.state = {
            email: "",
            password: ""
        }
    }  

    signUp(e) {
        e.preventDefault();        
        app.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
            .then((u) => {
                console.log('EXITOOOOOO CREASTE UN USUARIO');
            })
            .cath((err) => {
                console.log('Mal' + err.toString());
            });
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <div className="base-container">
                <div className="header">Register</div>
                <div className="content">
                    <div className="image">
                        
                    </div>
                    <div className="form">
                        <div className="form-group">
                            <label>Username</label>
                            <input 
                                type="text" 
                                name="username" 
                                />
                        </div>
                        <div className="form-group">
                            <label>Email</label>
                            <input 
                                type="email" 
                                name="email" 
                                placeholder="Ingrese email" 
                                onChange={this.handleChange}
                                value={this.state.email}
                                />
                        </div>
                        <div className="form-group">
                            <label>Password</label>
                            <input 
                                type="password" 
                                name="password" 
                                placeholder="Ingrese Password" 
                                onChange={this.handleChange}
                                value={this.state.password}
                                />
                        </div>
                    </div>
                </div>
                <div className="footer">
                    <button onClick={this.signUp} className="btn" type="button">Register</button>
                </div>
            </div>
        );
    }
}